﻿namespace ActiveTime
{
    partial class myActiveTimeOnPC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(myActiveTimeOnPC));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.atSecond = new System.Windows.Forms.Timer(this.components);
            this.btnReset = new System.Windows.Forms.Button();
            this.txtBxIdleTime = new System.Windows.Forms.TextBox();
            this.grpBxIdleTime = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtBxTotalIdleTime = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblActivePercent = new System.Windows.Forms.Label();
            this.idleTimer = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.txtbxDisplayCountTime = new System.Windows.Forms.TextBox();
            this.btnPause = new System.Windows.Forms.Button();
            this.History = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.grpBxIdleTime.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "1",
            "3",
            "5",
            "7",
            "10",
            "15",
            "30",
            "45",
            "60",
            "90",
            "120"});
            this.listBox1.Location = new System.Drawing.Point(14, 21);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 100);
            this.listBox1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Location = new System.Drawing.Point(638, 157);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(140, 127);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "InActivityTrigger";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 178);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 30);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // atSecond
            // 
            this.atSecond.Interval = 1000;
            this.atSecond.Tick += new System.EventHandler(this.atSecond_Tick);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(12, 250);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 30);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtBxIdleTime
            // 
            this.txtBxIdleTime.Enabled = false;
            this.txtBxIdleTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxIdleTime.Location = new System.Drawing.Point(6, 21);
            this.txtBxIdleTime.Multiline = true;
            this.txtBxIdleTime.Name = "txtBxIdleTime";
            this.txtBxIdleTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBxIdleTime.Size = new System.Drawing.Size(174, 55);
            this.txtBxIdleTime.TabIndex = 8;
            this.txtBxIdleTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // grpBxIdleTime
            // 
            this.grpBxIdleTime.Controls.Add(this.groupBox3);
            this.grpBxIdleTime.Controls.Add(this.groupBox2);
            this.grpBxIdleTime.Controls.Add(this.groupBox4);
            this.grpBxIdleTime.Location = new System.Drawing.Point(93, 33);
            this.grpBxIdleTime.Name = "grpBxIdleTime";
            this.grpBxIdleTime.Size = new System.Drawing.Size(685, 118);
            this.grpBxIdleTime.TabIndex = 9;
            this.grpBxIdleTime.TabStop = false;
            this.grpBxIdleTime.Text = "IdleTime";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtBxTotalIdleTime);
            this.groupBox3.Location = new System.Drawing.Point(198, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(226, 90);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Total Idle Time";
            // 
            // txtBxTotalIdleTime
            // 
            this.txtBxTotalIdleTime.Enabled = false;
            this.txtBxTotalIdleTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxTotalIdleTime.Location = new System.Drawing.Point(7, 21);
            this.txtBxTotalIdleTime.Multiline = true;
            this.txtBxTotalIdleTime.Name = "txtBxTotalIdleTime";
            this.txtBxTotalIdleTime.Size = new System.Drawing.Size(213, 55);
            this.txtBxTotalIdleTime.TabIndex = 0;
            this.txtBxTotalIdleTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBxTotalIdleTime.TextChanged += new System.EventHandler(this.txtBxTotalIdleTime_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtBxIdleTime);
            this.groupBox2.Location = new System.Drawing.Point(6, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(186, 90);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Last Activity (seconds)";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblActivePercent);
            this.groupBox4.Location = new System.Drawing.Point(430, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(189, 89);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Active %";
            // 
            // lblActivePercent
            // 
            this.lblActivePercent.AutoSize = true;
            this.lblActivePercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActivePercent.Location = new System.Drawing.Point(28, 20);
            this.lblActivePercent.Name = "lblActivePercent";
            this.lblActivePercent.Size = new System.Drawing.Size(133, 55);
            this.lblActivePercent.TabIndex = 10;
            this.lblActivePercent.Text = "XX.X";
            this.lblActivePercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // idleTimer
            // 
            this.idleTimer.Interval = 1000;
            this.idleTimer.Tick += new System.EventHandler(this.idleTimer_Tick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipText = "Running In System Tray";
            this.notifyIcon.BalloonTipTitle = "myActiveTimeOnPC";
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // txtbxDisplayCountTime
            // 
            this.txtbxDisplayCountTime.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtbxDisplayCountTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxDisplayCountTime.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtbxDisplayCountTime.Location = new System.Drawing.Point(93, 178);
            this.txtbxDisplayCountTime.Multiline = true;
            this.txtbxDisplayCountTime.Name = "txtbxDisplayCountTime";
            this.txtbxDisplayCountTime.ReadOnly = true;
            this.txtbxDisplayCountTime.Size = new System.Drawing.Size(539, 102);
            this.txtbxDisplayCountTime.TabIndex = 6;
            this.txtbxDisplayCountTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtbxDisplayCountTime.TextChanged += new System.EventHandler(this.txtbxDisplayCountTime_TextChanged);
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(11, 214);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 30);
            this.btnPause.TabIndex = 4;
            this.btnPause.Text = "PAUSE";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.button2_Click);
            // 
            // History
            // 
            this.History.Location = new System.Drawing.Point(93, 287);
            this.History.Name = "History";
            this.History.Size = new System.Drawing.Size(685, 262);
            this.History.TabIndex = 10;
            this.History.TabStop = false;
            this.History.Text = "History";
            // 
            // myActiveTimeOnPC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(790, 561);
            this.Controls.Add(this.History);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.txtbxDisplayCountTime);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpBxIdleTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "myActiveTimeOnPC";
            this.ShowInTaskbar = false;
            this.Text = "myActiveTimeOnPC";
            this.Load += new System.EventHandler(this.myActiveTimeOnPC_Load);
            this.groupBox1.ResumeLayout(false);
            this.grpBxIdleTime.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer atSecond;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox txtBxIdleTime;
        private System.Windows.Forms.GroupBox grpBxIdleTime;
        private System.Windows.Forms.Timer idleTimer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtBxTotalIdleTime;
        private System.Windows.Forms.Label lblActivePercent;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.TextBox txtbxDisplayCountTime;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.GroupBox History;
    }
}

