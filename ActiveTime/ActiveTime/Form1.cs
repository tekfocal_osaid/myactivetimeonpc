﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/////////////////////////////////////
//  Idle track
/////////////////////////////////////
using System.Runtime.InteropServices;


internal struct LASTINPUTINFO
{
    public uint cbSize;

    public uint dwTime;
}

namespace ActiveTime
{
    public partial class myActiveTimeOnPC : Form
    {
        private UInt32 un32TotalIdleTime = 0;
        private static UInt32 un32IdleLast = 0;
        private UInt64 un64TimeSinceStart = 0;
        private double fActivePercent = 0.0F;

        public myActiveTimeOnPC()
        {
            InitializeComponent();
            txtbxDisplayCountTime.TextAlign = HorizontalAlignment.Right;
            txtbxDisplayCountTime.Text = "00:00:00";
            btnReset.Enabled = false;
            //txtBxIdleTime.TextAlign = HorizontalAlignment.Center;
            //Microsoft Sans Serif, 72pt
            //txtBxIdleTime.Font = new Font("Microsoft Sans Serif", 48, FontStyle.Bold);
            txtBxIdleTime.Text = "0";
            txtBxTotalIdleTime.Text = "0";
        }



        private void atSecond_Tick(object sender, EventArgs e)
        {
          double seconds = TimeSpan.Parse(txtbxDisplayCountTime.Text).TotalSeconds;

          UInt64 userVal = (UInt64)seconds + 1;
          TimeSpan startTime = TimeSpan.FromSeconds(userVal);
          txtbxDisplayCountTime.Text = startTime.ToString(@"hh\:mm\:ss");
          un64TimeSinceStart++;
        }

        private void txtbxDisplayCountTime_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {            
            atSecond.Enabled = true;
            idleTimer.Enabled = true;
            btnPause.Enabled = true;
            btnStart.Enabled = false;
            btnReset.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            atSecond.Enabled = false;
            idleTimer.Enabled = false;
            btnStart.Enabled = true;
            btnPause.Enabled = false;
        }

        private void myActiveTimeOnPC_Load(object sender, EventArgs e)
        {
            btnPause.Enabled = false;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            atSecond.Enabled = false;
            idleTimer.Enabled = false;
            btnStart.Enabled = true;
            btnPause.Enabled = false;
            btnReset.Enabled = false;
            txtbxDisplayCountTime.Text = "00:00:00";
            txtBxTotalIdleTime.Text    = "00:00:00";
            lblActivePercent.Text = "XX.X";
            txtBxIdleTime.Text = "X";
            un32TotalIdleTime = 0;
        }

        private void idleTimer_Tick(object sender, EventArgs e)
        {
            UInt32 un32IdleNow = IdleTimeFinder.GetIdleTime();
            un32IdleNow = un32IdleNow / 1000;
            txtBxIdleTime.Text = un32IdleNow.ToString();

            // if greater than one second
            if (un32IdleLast - un32IdleNow >= 1)
            {
                un32TotalIdleTime++;
                TimeSpan idlTime = TimeSpan.FromSeconds(un32TotalIdleTime);
                txtBxTotalIdleTime.Text = idlTime.ToString(@"hh\:mm\:ss");
               // txtBxTotalIdleTime.Text = un32TotalIdleTime.ToString();
            }

            if (un64TimeSinceStart==0UL) { un64TimeSinceStart = 1; }

            fActivePercent = ((double)(un64TimeSinceStart - un32TotalIdleTime) / (double)un64TimeSinceStart) * 100.0F;
            lblActivePercent.Text = String.Format("{0:F1}", fActivePercent);

            un32IdleLast = un32IdleNow;
        }

        private void txtBxTotalIdleTime_TextChanged(object sender, EventArgs e)
        {

        }

        private void myActiveTimeOnPC_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon.Visible = true;
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon.Visible = false;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;   
        }

    }
}

// <summary>
/// Helps to find the idle time, (in milliseconds) spent since the last user input
/// </summary>
public class IdleTimeFinder
{
    [DllImport("User32.dll")]
    private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

    [DllImport("Kernel32.dll")]
    private static extern uint GetLastError();

    public static uint GetIdleTime()
    {
        LASTINPUTINFO lastInPut = new LASTINPUTINFO();
        lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
        GetLastInputInfo(ref lastInPut);

        return ((uint)Environment.TickCount - lastInPut.dwTime);
    }
    /// <summary>
    /// Get the Last input time in milliseconds
    /// </summary>
    /// <returns></returns>
    public static long GetLastInputTime()
    {
        LASTINPUTINFO lastInPut = new LASTINPUTINFO();
        lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
        if (!GetLastInputInfo(ref lastInPut))
        {
            throw new Exception(GetLastError().ToString());
        }
        return lastInPut.dwTime;
    }
}
