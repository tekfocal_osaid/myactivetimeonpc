Goal:
Track user active time on computer -> hinting activity during office hours.

Description:
The application shall track inactivity time. This shall be done by guaging 
user inactivity by starting inactivity clock after configurable inactivity threshold.
 
Primary 
=======
Phase 1
Configurable option
1. Inactivity count start = [ 1 minute ~ 255 (4.25hrs)]
2. Start/Pause and Stop button
3. Total measuring time shown on screen
4. Total Active time shown along with percentage. (refreshed every minute)

Phase 2
1. Get inactivity Trigger from user and start inactivity timer based on it.
2. Stop inactivity counce once active again and wait for trigger time to expire before counting agin.
3. Show Start time 

BUG: 
1. Maj: Goes missing when win + D is pressed. Does not come back out even from system tray. Comes out using win + Tab 
2. Min: At times, does not comes on top when double clicked in the system tray.


Phase 3
1. Track time within office hours. Configure office hours
2. Send email at end of Day/week/month/year/FY of progress



Secondary
=========

1. Keyboard/mouse Concentration bar graphs
minute bar counts every second on activity. Graph will show how active was the user
every minute. At every second if there was mouse movement or keyboard hits the 
bar count will increment for that minute. so max of 60 counts and min 0.
 
2. Fine tune in Seconds
 




